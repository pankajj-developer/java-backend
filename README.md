# Spring boot (version 2.5.4) based web application for User Text Management

Following dependencies are added for this application - 

1. Web
2. jpa (hibernate)
3. H2 (in memory Database)


## Running Application on local environment

java -jar java-backend-0.0.1-SNAPSHOT.jar

## Rest API available are - 


1. Get All Text (GET API)- 

	'localhost:9000/assignment/user-text/find-all-user-text'
	
2. Save New Text (POST API)-
	
	'localhost:9000/assignment/user-text/save'
	
	Below data structure has to pass in POST Body - 
		{text: 'Sample Text'
		}
	
3. Delete Text (DELETE API)- Text id has to be pass as path variable for this API

	'localhost:9000/assignment/user-text/delete/{textId}'
	
	
Text structure -
	{ text:'Sample Text',
	  id: 1
	}
	
## H2 Database Url - 

1. Navigate to 'http://localhost:9000/assignment/h2-console'
2. enter jdbc:h2:mem:testassignmentdb as JDBC Url
3. click on Connect button
	
	
 