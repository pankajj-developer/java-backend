package com.assignment.text.exception;

import java.io.Serializable;

public class ExceptionResponse implements Serializable {

	private String errorMessage;
	private String requestedUri;

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getRequestedUri() {
		return requestedUri;
	}

	public void setRequestedUri(String requestedUri) {
		this.requestedUri = requestedUri;
	}

}
