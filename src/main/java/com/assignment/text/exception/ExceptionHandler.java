package com.assignment.text.exception;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.util.HtmlUtils;

@ControllerAdvice
public class ExceptionHandler {

	@org.springframework.web.bind.annotation.ExceptionHandler(Exception.class)
	@ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR)
	public @ResponseBody ExceptionResponse handleException(Exception ex, HttpServletRequest request) {
		ExceptionResponse exceptionRes = new ExceptionResponse();
		exceptionRes.setErrorMessage(ex.getMessage());
		exceptionRes.setRequestedUri(HtmlUtils.htmlEscape(request.getRequestURI()));
		return exceptionRes;
	}
}
