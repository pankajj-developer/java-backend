package com.assignment.text.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.assignment.text.model.UserTextDTO;
import com.assignment.text.service.api.IUserTextService;

@RestController()
@CrossOrigin
public class UserTextController {

	@Autowired
	private IUserTextService userTextService;

	@GetMapping("/user-text/find-all-user-text")
	public List<UserTextDTO> getUserTexts() {
		return userTextService.findAll();
	}

	@PostMapping("/user-text/save")
	public UserTextDTO saveUserText(@RequestBody UserTextDTO dto) {
		return userTextService.save(dto);
	}

	@DeleteMapping("/user-text/delete/{id}")
	public void deleteUserText(@PathVariable Long id) {
		userTextService.delete(id);
	}
}
