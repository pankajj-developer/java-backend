package com.assignment.text.service.api;

import java.util.List;

import com.assignment.text.model.UserTextDTO;

public interface IUserTextService {

	List<UserTextDTO> findAll();

	void delete(Long id);
	
	UserTextDTO save(UserTextDTO userText);

}
