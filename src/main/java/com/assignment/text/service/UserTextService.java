package com.assignment.text.service;

import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.assignment.text.entity.UserText;
import com.assignment.text.model.UserTextDTO;
import com.assignment.text.repository.UserTextRepository;
import com.assignment.text.service.api.IUserTextService;

@Service
@Transactional
public class UserTextService implements IUserTextService {

	@Autowired
	private UserTextRepository userTextRepository;

	@Override
	public List<UserTextDTO> findAll() {
		List<UserText> allUserText = userTextRepository.findAll();
		if(!allUserText.isEmpty()) {
			List<UserTextDTO> dtos = allUserText.stream().map(new Function<UserText, UserTextDTO>() {
			    @Override
			    public UserTextDTO apply(UserText userText) {
			        return convertUserText(userText);
			    }
			}).collect(Collectors.toList());
			return dtos;
		}
		return Collections.emptyList();
	}

	@Override
	public void delete(Long id) {
		userTextRepository.deleteById(id);
	}

	@Override
	public UserTextDTO save(UserTextDTO userTextDTO) {
		UserText userText = new UserText();
		userText.setText(userTextDTO.getText());
		UserText userTextSaved = userTextRepository.save(userText);
		userTextDTO.setId(userTextSaved.getId());
		return userTextDTO;
	}
	
	private UserTextDTO convertUserText(UserText userText) {
		UserTextDTO dto = new UserTextDTO();
		dto.setId(userText.getId());
		dto.setText(userText.getText());
		return dto;
	}

}
