package com.assignment.text.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.assignment.text.entity.UserText;

public interface UserTextRepository extends JpaRepository<UserText, Long> {

}
